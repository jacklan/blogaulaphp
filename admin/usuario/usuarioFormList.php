<?php

#### funcao que exibe erro da pagina ####
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

###### INCLUI PAGINAS ######
include './UsuarioDB.php';
$db = Conexao::abrir();
include '../funcoesPagina.php';

##### Verifica usuario Logado ####
verificarLogin();


###### CHAMA FUNCAO TOPO PAGINA###### 
chamarTopoPagina();

?>

<body>
    <div class="container">
        <?php
        ############ Inicio Menu #########
        chamarMenu();//funcao chama menu
        ############ fim Menu ############
        #
        ########################## INICIO ACOES PAGINA ###############################
        #
        ########################### INICIO INSERIR DADOS ###############################
        // função 'isset' verifica se existe valor no vetor da variavel $_POST enviar 
        if (isset($_POST['enviar'])) {
            //se exister valor pega o nome e email do form e armazena nas variaveis $nome e $email
            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $idade = $_POST['idade'];
            $login = $_POST['login'];
            $senha = $_POST['senha'];

            //chamar funcao salvar
            salvar($nome, $email, $idade, $login, $senha);
        }
        #
        ############################ FIM INSERIR DADOS ###############################
        #
        #
        ############################# INICIO ATUALIZAR DADOS #########################
        #
        // se clicar  no botão alterar vem para esta tela
        if (isset($_POST['btnAtualizar'])) {
            $id = (int) $_GET['id'];
            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $idade = $_POST['idade'];
            $login = $_POST['login'];
            $senha = $_POST['senha'];

            //funcao atualizar
            atualizar($nome, $email, $idade, $login,$senha,$id);
        }
        ######################### FIM ATUALIZAR DADOS ################################
        #
        #
        ######################### INICIO DELETAR DADOS ###############################

        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $id = (int) $_GET['id'];

            //chamar funcao deletar
            deletar($id);
        }
        ######################### FIM DELETAR DADOS ###############################
        ?>

    <article>

        <section class="jumbotron">
            <?php
            ######################### INICIO FORMULARIO ATUALIZAR #######################################
            if (isset($_GET['action']) && $_GET['action'] == 'update') {

                $id = (int) $_GET['id'];

               $result = carregarUsuario($id);
                ?>

                <ul class="breadcrumb">
                    <li><a href="../index.php">Página inicial <span class="divider"> /</span> </a></li>
                    <li class="active">Atualizar</li>
                </ul>
                <h3>Formulario Usuario</h3>
                <form method="post" action="">
                     <label>Nome</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input type="text" name="nome" value="<?php echo $result->nome; ?>"/>
                    </div>
                    <label>E-mail</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="email" value="<?php echo $result->email; ?>" />
                    </div>
                    <label>Idade</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="idade" value="<?php echo $result->idade; ?>" />
                    </div>
                    <label>Login</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="login" value="<?php echo $result->login; ?>" />
                    </div>
                    <label>Senha</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="password" name="senha" value="<?php echo $result->senha; ?>" />
                    </div>
                    <br />
                    <input type="submit" name="btnAtualizar" class="btn btn-primary" value="Atualizar dados">					
                </form>

            <?php 
            ######################### FIM FORMULARIO ATUALIZAR  ##########################
            #
            #
            ######################### INICIO FORMULARIO CADASTRAR #######################
            
            } else {
                
                ?>
                <h3>Formulario Usuario</h3>
                <form method="post" action="">
                    <label>Nome</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input type="text" name="nome" placeholder="Nome:" />
                    </div>
                    <label>E-mail</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="email" placeholder="E-mail:" />
                    </div>
                    <label>Idade</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="idade" placeholder="idade:" />
                    </div>
                    <label>Login</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="login" placeholder="login:" />
                    </div>
                    <label>Senha</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="password" name="senha" placeholder="*****" />
                    </div>
                    <br />
                    <input type="submit" name="enviar" class="btn btn-primary" value="Cadastrar dados">					
                </form>

            <?php
             }//fim
             ######################### FIM FORMULARIO CADASTRAR ####################
             #
             #
             ######################### INICIO LISTAGEM DADOS #######################
                    
            ?>
            <table class="table table-hover">   
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome:</th>
                        <th>E-mail:</th>
                        <th>Idade:</th>
                        <th>Login:</th>
                        <th>Ações:</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                   ######### INICIO CARREGAR TODOS OS DADOS #########
                    //carrega todos os dados da tabela banco de dados
                    $sqlRead = 'SELECT * FROM tb_usuario order by id desc';
                    try {
                        $read = $db->prepare($sqlRead);
                        $read->execute();
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                    }
                    while ($rs = $read->fetch(PDO::FETCH_OBJ)) {
                        ?>
                        <tr>
                            <td><?php echo $rs->id; ?></td>
                            <td><?php echo $rs->nome; ?></td>
                            <td><?php echo $rs->email; ?></td>
                            <td><?php echo $rs->idade; ?></td>
                            <td><?php echo $rs->login; ?></td>
                            <td>
                                <a href="usuarioFormList.php?action=update&id=<?php echo $rs->id; ?>" class="btn"><i class="icon-pencil"></i></a>
                                <a href="usuarioFormList.php?action=delete&id=<?php echo $rs->id; ?>" class="btn" onclick="return confirm('Deseja deletar?');"><i class="icon-remove"></i></a>
                            </td>
                        </tr>
                    <?php
                    }
                    ####### FIM CARREGAR DADOS #######
                    ?>
                </tbody>
            </table>
            <!----------------------- FIM LISTAGEM DADOS  ----------------------->
        </section>

    </article>

</div>
<?php 
###### CHAMA FUNCAO FIM PAGINA ######
chamarRodape();
