<?php

//session_start();

function chamarTopoPagina() {
    ?>
    <!DOCTYPE HTML>
    <html land="pt-BR">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>Projeto Base TCC - Tecnico Informática EAJ 2014.2</title>
            <meta name="description" content="" />
            <meta name="robots" content="index, follow" />
            <meta name="author" content=""/>
            <link rel="stylesheet" href="../css/bootstrap.css" />
            <link rel="stylesheet" type="text/css"  href="../css/menu.css" />
            <link rel="stylesheet" />
            <!--[if lt IE 9]>
                <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
             <![endif]-->
        </head>
        <?php
    }

    function chamarMenu() {
        ?>
        <h2 class="muted"><a href="../index.php"><img src="../img/logophp.png" style="max-width: 100px;"/></a>Projeto Base TCC - Tecnico Informática EAJ 2014.2</h2>
        <nav class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav menu">
                        <li><a href="../index.php">Página inicial</a></li>
                        <li><a href="../usuario/usuarioFormList.php">Usuario</a></li>
                        <li><a href="#">Conteudo</a>
                            <ul>
                                <li><a href="../categoria/categoriaFormList.php">Categoria</a></li>
                                <li><a href="../post/postFormList.php">Post</a></li>
                            </ul>
                        </li>
                        <li><a href="../contato/contatoFormList.php">Form Contato</a></li>
                        <li><a href="../slideshow/slideshowFormList.php">SlideShow</a></li>
                        <li><a href="../login.php?sair=1">Sair</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php
    }

    function chamarRodape() {
        ?>
        <script src="../js/jQuery.js"></script>
        <script src="../js/bootstrap.js"></script>
    </body>
    </html>
    <?php
}

function dataus($date) {
    if ($date) {
        // get the date parts
        $day = substr($date, 0, 2);
        $mon = substr($date, 3, 2);
        $year = substr($date, 6, 4);
        return "{$year}-{$mon}-{$day}";
    }
}

/**
 * Convert a date to format dd/mm/yyyy
 * @param $date = date in format yyyy-mm-dd
 */
function databr($date) {
    if ($date) {
        // get the date parts
        $year = substr($date, 0, 4);
        $mon = substr($date, 5, 2);
        $day = substr($date, 8, 2);
        return "{$day}/{$mon}/{$year}";
    }
}

function verificarLogin() {

    ini_set('session.save_path', realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/blogtcc/admin/session'));
    session_start();

    if ($_SESSION['login'] == NULL || filter_input(INPUT_GET, 'sair') == 1) {
        session_destroy();
        $_SESSION['login'] = NULL;
        //header("Location: http://" . $_SERVER['HTTP_HOST'] . dirname(dirname($_SERVER['SCRIPT_NAME'])) . DIRECTORY_SEPARATOR . "login.php");
        header("Location: http://" . $_SERVER['HTTP_HOST'] . "/blogtcc/admin/login.php");
    }
}
