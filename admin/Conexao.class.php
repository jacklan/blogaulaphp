<?php
class Conexao {

    public static function abrir() {
        $host = "localhost";
        $dbnome = "db_name";
        $usuario = "user";
        $senha = "******";
        $tipoBanco = "mysql";

        $conn = $tipoBanco . ":host=" . $host . ";dbname=" . $dbnome;
        try {
            $db = new PDO($conn, $usuario, $senha);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//        $_SESSION['db'] = $db;
        } catch (PDOException $e) {
            if ($e->getCode() == 1049) {
                echo "Erro no banco de dados.";
            } else {
                echo $e->getMessage();
            }
        }
        return $db;
    }

}
?>