<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

//include dirname(__FILE__) . DIRECTORY_SEPARATOR . "Conexao.class.php";

include './usuario/UsuarioDB.php';
$db = Conexao::abrir();

ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/blogtcc/admin/session'));
session_start();

$_SESSION['login'] = null;
?>
<!DOCTYPE HTML>
<html land="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Projeto Base TCC - Tecnico Informática EAJ 2014.2</title>
        <meta name="description" content="" />
        <meta name="robots" content="index, follow" />
        <meta name="author" content=""/>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css"  href="css/menu.css" />
        <link rel="stylesheet" />
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
         <![endif]-->
    </head>
    <body>
        <div class="container">
            <?php
            if (isset($_POST['logar'])) {

                $login = $_POST['login'];
                $senha = $_POST['senha'];

                $sqlSelect = 'SELECT * FROM tb_usuario WHERE login = :login AND senha = :senha';

                try {
                    $select = $db->prepare($sqlSelect);
                    $select->bindValue(':login', $login, PDO::PARAM_STR);
                    $select->bindValue(':senha', $senha, PDO::PARAM_STR);
                    $select->execute();

                    $objUsuario = $select->fetch(PDO::FETCH_OBJ);
                } catch (PDOException $e) {
                    echo "<div class='alert alert-error'>
                            <button type='button' class='close' data-dismiss='alert'>&times;</button>
                            <strong>Login ou Senha errado! " . $e->getMessage() . "</strong>
                            </div>";
                }
                if (!empty($objUsuario) && ($login == $objUsuario->login && $senha == $objUsuario->senha)) {
                    $_SESSION['login'] = $login;
                    header("Location: http://" . $_SERVER['HTTP_HOST'] . "/blogtcc/admin/index.php");
                    //  header("Location: http://" . $_SERVER['HTTP_HOST'] . dirname("/".$_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR . 'index.php');
                } else {
                    $_SESSION['login'] = null;
                    echo "<div class='alert alert-error'>
                            <button type='button' class='close' data-dismiss='alert'>&times;</button>
                            <strong>Login ou Senha errado!</strong>
                            </div>";
                }
            }
            ?>
            <h2 class="muted"><a href="index.php"><img src="img/logophp.png" style="max-width: 100px;"/></a> Projeto Base TCC - Tecnico Informática EAJ 2014.2</h2>
            <!-- inicio conteudo -->
            <article>

                <section class="jumbotron">
                    <center><h3>Bem vindo ao sistema de gerenciamento para sites.</h3>
                        <form action="" method="post">
                            <input type="text" name="login" required="" placeholder="Login"/><br>
                            <input type="password" name="senha" required="" placeholder="Senha"/><br>
                            <input type="submit" name="logar" value="Entrar"/>

                        </form>
                        <div><a href="../index.php"><b>Voltar</b></a></div>
                    </center>
                </section>
            </article>
            <!-- fim conteudo -->
        </div>
        <script src="js/jQuery.js"></script>
        <script src="js/bootstrap.js"></script>
    </body>
</html>