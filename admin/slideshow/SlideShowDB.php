<?php

###### INCLUI PAGINAS ######
include dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "Conexao.class.php";

############## pagina funcoes de trasacao com banco de dados Post.##########
#
######################### inicio Salvar #######################################

function salvar($titulo, $descricao, $arquivo_foto, $link, $publicar) {

    if (!empty($arquivo_foto['name'])) {
        $nome_imagem = salvarImagem($arquivo_foto);
    }

    $db = Conexao::abrir();
    //comando SQL do banco de dados para inserir registros
    $sql = 'INSERT INTO tb_slideshow (titulo, descricao,arquivo_foto,link,publicar) VALUES (:titulo, :descricao,:arquivo_foto,:link,:publicar)';
    //try catch tratamento de exceção
    try {
        //variavel $db criada na pagina Conexao.class.php sendo chamada nesta pagina
        //pega o valor da variavel SQL passando como parametro na função prepare
        $create = $db->prepare($sql);
        //chama a função bindValue passando o parametro titulo e descricao do comando SQL e o valor da variavel
        $create->bindValue(':titulo', $titulo, PDO::PARAM_STR);
        $create->bindValue(':descricao', $descricao, PDO::PARAM_STR);
        //  $create->bindValue(':arquivo_foto', $conteudo_foto, PDO::PARAM_LOB);
        $create->bindValue(':arquivo_foto', $nome_imagem, PDO::PARAM_STR);
        $create->bindValue(':link', $link, PDO::PARAM_STR);
        $create->bindValue(':publicar', $publicar, PDO::PARAM_INT);
        // execulta os valores passados no parametro
        if ($create->execute()) {
            //mostra um alerta na tela de sucesso
            echo "<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Inserido com sucesso!</strong>
                    </div>";
        }
    } catch (PDOException $e) {
        // se caso de algum erro vem para o tratamento de exceção
        echo "<div class='alert alert-error'>
                <button type='button' class='close' data-dismiss='alert'>&times;</button>
                <strong>Erro ao inserir dados!</strong>" . $e->getMessage() . "
                </div>";
    }
}

######################### fim Salvar ##########################################
#
#
######################### inicio Atualizar ####################################

function atualizar($titulo, $descricao, $arquivo_foto, $link, $publicar, $id) {

    $db = Conexao::abrir();
    $objImagem = carregarSlideShow($id);

    if (file_exists('imagens/' . $objImagem->arquivo_foto) && !empty($arquivo_foto['name'])) {

        unlink('imagens/' . $objImagem->arquivo_foto); //remove a foto
        $nome_imagem = salvarImagem($arquivo_foto); //salve a nova foto
        //comando SQL para atualização do formulario
        $sqlUpdate = 'UPDATE tb_slideshow SET titulo = ?, descricao = ?, arquivo_foto = ?, link = ?, publicar = ? WHERE id = ?';
        //cria um array recebendo os parametros do formulario de acordo com a sequencia do comando SQL
        $dados = array($titulo, $descricao, $nome_imagem, $link, $publicar, $id);
    } elseif (empty($arquivo_foto['name'])) {
        //comando SQL para atualização do formulario
        $sqlUpdate = 'UPDATE tb_slideshow SET titulo = ?, descricao = ?, link = ?, publicar = ? WHERE id = ?';
        //cria um array recebendo os parametros do formulario de acordo com a sequencia do comando SQL
        $dados = array($titulo, $descricao, $link, $publicar, $id);
    }

    try {
        //
        $update = $db->prepare($sqlUpdate);
        if ($update->execute($dados)) {
            echo "<div class='alert alert-success'>
                        <button type='button' class='close' data-dismiss='alert'>&times;</button>
                        <strong>Atualizado com sucesso!</strong>
                        </div>";
        }
    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Erro ao atualizar!</strong>" . $e->getMessage() . "
                    </div>";
    }
}

######################### fim Atualizar #######################################
#
#
######################### inicio Deletar ######################################

function deletar($id) {

    $db = Conexao::abrir();
    $sqlDelete = 'DELETE FROM tb_slideshow WHERE id = :id';

    $objImagem = carregarSlideShow($id); // funcao retorna o nome da foto 

    if (file_exists('imagens/' . $objImagem->arquivo_foto)) {//verifica se existe a foto
        unlink('imagens/' . $objImagem->arquivo_foto); //remove a foto
    }

    try {
        $delete = $db->prepare($sqlDelete);
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        if ($delete->execute()) {
            echo "<div class='alert alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>&times;</button>
                            <strong>Deletado com sucesso!</strong>
                            </div>";
            header('Location: slideshowFormList.php');
        }
    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Erro ao deletar!</strong>" . $e->getMessage() . "
                    </div>";
    }
}

######################### fim Deletar #######################################
#
#
######################### Salvar foto #######################################

function salvarImagem($arquivo_foto) {
    try {
        //salvar foto em diretorio
        $_FILES['arquivo_foto'] = $arquivo_foto; // armazena variavel em $_files

        if (isset($_FILES['arquivo_foto'])) {// verifica se existe uma foto
            date_default_timezone_set("America/Fortaleza"); //Definindo timezone padrão

            $ext = strtolower(substr($_FILES['arquivo_foto']['name'], -4)); //Pegando extensão do arquivo
            $nome_imagem = date("d_m_Y_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . "imagens" . DIRECTORY_SEPARATOR; //Diretório para uploads

            move_uploaded_file($_FILES['arquivo_foto']['tmp_name'], $dir . $nome_imagem); //Fazer upload do arquivo
        }
        return $nome_imagem;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

#
#
######################### fim Salvar foto ################################
######################### inicio Carregar um SlideShow ###################

function carregarSlideShow($id) {

    $db = Conexao::abrir();
    $sqlSelect = 'SELECT * FROM tb_slideshow WHERE id = :id';

    try {
        $select = $db->prepare($sqlSelect);
        $select->bindValue(':id', $id, PDO::PARAM_INT);
        $select->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    $result = $select->fetch(PDO::FETCH_OBJ);

    return $result;
}

#
######################### fim Carregar um SlideShow #######################
