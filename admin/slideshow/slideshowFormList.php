<?php
#### funcao que exibe erro da pagina ####
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
###### INCLUI PAGINAS ######
include './SlideShowDB.php';
$db = Conexao::abrir();
include '../funcoesPagina.php';

##### Verifica usuario Logado ####
verificarLogin();

###### CHAMA FUNCAO TOPO PAGINA###### 
chamarTopoPagina();
?>

<body>
    <div class="container">
        <?php
############ Inicio Menu #########
        chamarMenu(); //funcao chama menu
############ fim Menu ############
#
########################## INICIO ACOES PAGINA ###############################
#
########################### INICIO INSERIR DADOS ###############################
        // função 'isset' verifica se existe valor no vetor da variavel $_POST enviar 
        if (isset($_POST['enviar'])) {
            //se exister valor pega o titulo e descricao do form e armazena nas variaveis $titulo e $descricao
            $titulo = $_POST['titulo'];
            $descricao = $_POST['descricao'];
            $arquivo_foto = $_FILES['arquivo_foto'];
            $link = $_POST['link'];

            if (empty($_POST['publicar'])) {
                $publicar = 0;
            } else {
                $publicar = $_POST['publicar'];
            }

            //chamar funcao salvar
            salvar($titulo, $descricao, $arquivo_foto, $link, $publicar);
        }
#
############################ FIM INSERIR DADOS ###############################
#
#
############################# INICIO ATUALIZAR DADOS #########################
#
        // se clicar  no botão alterar vem para esta tela
        if (isset($_POST['btnAtualizar'])) {
            $id = (int) $_GET['id'];
            $titulo = $_POST['titulo'];
            $descricao = $_POST['descricao'];
            $arquivo_foto = $_FILES['arquivo_foto'];
            $link = $_POST['link'];

            if (empty($_POST['publicar'])) {
                $publicar = 0;
            } else {
                $publicar = $_POST['publicar'];
            }

            //funcao atualizar
            atualizar($titulo, $descricao, $arquivo_foto, $link, $publicar, $id);
        }
######################### FIM ATUALIZAR DADOS ################################
#
#
######################### INICIO DELETAR DADOS ###############################

        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $id = (int) $_GET['id'];

            //chamar funcao deletar
            deletar($id);
        }
######################### FIM DELETAR DADOS ###############################
        ?>

        <article>

            <section class="jumbotron">
                <?php
######################### INICIO FORMULARIO ATUALIZAR #######################################
                if (isset($_GET['action']) && $_GET['action'] == 'update') {

                    $id = (int) $_GET['id'];

                    $result = carregarSlideShow($id);
                    ?>

                    <ul class="breadcrumb">
                        <li><a href="../index.php">Página inicial <span class="divider"> /</span> </a></li>
                        <li class="active">Atualizar</li>
                    </ul>
                    <h3>Formul&aacute;rio SlideShow</h3>
                    <!-- <form method="post" action="" enctype="multipart/form-data"> -->
                    <form method="post" action="" enctype="multipart/form-data">
                        <label>Titulo</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input type="text" name="titulo" placeholder="Titulo"  value="<?php echo $result->titulo; ?>"/>
                        </div>
                        <label>Descri&ccedil;&atilde;o</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <textarea name="descricao" placeholder="Descrição..." rows="3"  cols="5" value="<?php echo $result->descricao; ?>" ><?php echo $result->descricao; ?></textarea>
                        </div>
                        <div class="input-prepend" style="float: left; margin-left: 50%; margin-top: -200px;">
                            <?php
                            if (file_exists('imagens/' . $result->arquivo_foto) && $result->arquivo_foto != "") {
                                ?>
                                <img src="imagens/<?php echo $result->arquivo_foto; ?>" name="arquivo_foto" style="width: 350px; height: 250px; max-width: 100%;" />
                            <?php } else {
                                ?>
                                <img src="imagens/<?php echo 'sem_imagem.jpg' ?>" name="arquivo_foto" style="width: 350px; height: 250px; max-width: 100%;" />
                                <?php
                            }
                            ?>
                            <br>
                            <br>
                            <input type="file" name="arquivo_foto" />
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label>Linkar</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="link" name="link" placeholder="Insira um link" value="<?php echo $result->link; ?>" />
                        </div>
                        <br />
                        <label>Publicar</label>Sim ou n&atilde;o
                        <div class="input-prepend">
                            <?php
                            if ($result->publicar == 1 && $result->publicar != null) {
                                ?>
                                <input type="checkbox" name="publicar" checked="checked" class="btn btn-block" value="1"/>

                            <?php } else { ?>

                                <input type='checkbox' name='publicar' class='btn btn-block' value="1"/>

                            <?php } ?>
                        </div>
                        <br />
                        <input type="submit" name="btnAtualizar" class="btn btn-primary" value="Atualizar dados">					
                    </form>

                    <?php
                    ######################### FIM FORMULARIO ATUALIZAR  ##########################
                    #
            #
            ######################### INICIO FORMULARIO CADASTRAR #######################
                } else {
                    ?>
                    <h3>Formul&aacute;rio SlideShow</h3>
                    <form method="post" action="" enctype="multipart/form-data">
                        <label>Titulo</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input type="text" name="titulo" placeholder="Titulo" required="required" />
                        </div>
                        <label>Descri&ccedil;&atilde;o</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <textarea name="descricao" placeholder="Descrição..." rows="3"  cols="5"></textarea>
                        </div>
                        <label>Imagem</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input type="file" name="arquivo_foto"  required="required" />

                        </div>
                        <label>Linkar</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="link" name="link" placeholder="Insira um link" />
                        </div>
                        <br />
                        <label>Publicar</label>Sim ou n&atilde;o
                        <div class="input-prepend">
                            <input type="checkbox" name="publicar" class="btn btn-block" value="1">				
                        </div><br />
                        <input type="submit" name="enviar" class="btn btn-primary" value="Cadastrar dados">					
                    </form>

                    <?php
                }//fim
                ######################### FIM FORMULARIO CADASTRAR ####################
                #
             #
             ######################### INICIO LISTAGEM DADOS #######################
                ?>
                <table class="table table-hover">   
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titulo:</th>
                            <th>Descri&ccedil;&atilde;o:</th>
                            <th>Foto:</th>
                            <th>Publicado:</th>
                            <th>Ações:</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        ######### INICIO CARREGAR TODOS OS DADOS #########
                        //carrega todos os dados da tabela banco de dados
                        $sqlRead = 'SELECT * FROM tb_slideshow order by id desc';
                        try {
                            $read = $db->prepare($sqlRead);
                            $read->execute();
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                        }
                        while ($rs = $read->fetch(PDO::FETCH_OBJ)) {
                            ?>
                            <tr>
                                <td><?php echo $rs->id; ?></td>
                                <td><?php echo $rs->titulo; ?></td>
                                <td><?php echo $rs->descricao ?></td>
                                <td><?php
                                    if (file_exists('imagens/' . $rs->arquivo_foto) && $rs->arquivo_foto != "") {
                                        ?>
                                        <img src="imagens/<?php echo $rs->arquivo_foto; ?>" name="arquivo_foto" style="width: 150px; height: 120px; max-width: 100%;" />
                                    <?php } else {
                                        ?>
                                        <img src="imagens/<?php echo 'sem_imagem.jpg' ?>" name="arquivo_foto" style="width: 150px; height: 120px; max-width: 100%;" />
                                        <?php
                                    }
                                    ?>
                                <td><?php echo $rs->publicar; ?></td>
                                <td>
                                    <a href="slideshowFormList.php?action=update&id=<?php echo $rs->id; ?>" class="btn"><i class="icon-pencil"></i></a>
                                    <a href="slideshowFormList.php?action=delete&id=<?php echo $rs->id; ?>" class="btn" onclick="return confirm('Deseja deletar?');"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ####### FIM CARREGAR DADOS #######
                        ?>
                    </tbody>
                </table>
                <!----------------------- FIM LISTAGEM DADOS  ----------------------->
            </section>

        </article>

    </div>
    <?php
###### CHAMA FUNCAO FIM PAGINA ######
    chamarRodape();
    