<?php
###### INCLUI PAGINAS ######
include "../Conexao.class.php";

############## pagina funcoes de trasacao com banco de dados Usuario.##########
#
######################### inicio Salvar #######################################

function salvar($nome) {

    $db = Conexao::abrir();
    //comando SQL do banco de dados para inserir registros
    $sql = 'INSERT INTO tb_categoria (nome) VALUES (:nome)';
    //try catch tratamento de exceção
    try {
        //variavel $db criada na pagina Conexao.class.php sendo chamada nesta pagina
        //pega o valor da variavel SQL passando como parametro na função prepare
        $create = $db->prepare($sql);
        //chama a função bindValue passando o parametro nome e email do comando SQL e o valor da variavel
        $create->bindValue(':nome', $nome, PDO::PARAM_STR);
        // execulta os valores passados no parametro
        if ($create->execute()) {
            //mostra um alerta na tela de sucesso
            echo "<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Inserido com sucesso!</strong>
                    </div>";
        }
    } catch (PDOException $e) {
        // se caso de algum erro vem para o tratamento de exceção
        echo "<div class='alert alert-error'>
                <button type='button' class='close' data-dismiss='alert'>&times;</button>
                <strong>Erro ao inserir dados!</strong>" . $e->getMessage() . "
                </div>";
    }
}

######################### fim Salvar ##########################################
#
#
######################### inicio Atualizar ####################################

function atualizar($nome, $id) {

    $db = Conexao::abrir();
    //comando SQL para atualização do formulario
    $sqlUpdate = 'UPDATE tb_categoria SET nome = ? WHERE id = ?';
    //cria um array recebendo os parametros do formulario de acordo com a sequencia do comando SQL
    $dados = array($nome, $id);
    try {
        //
        $update = $db->prepare($sqlUpdate);
        if ($update->execute($dados)) {
            echo "<div class='alert alert-success'>
                        <button type='button' class='close' data-dismiss='alert'>&times;</button>
                        <strong>Atualizado com sucesso!</strong>
                        </div>";
        }
    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Erro ao atualizar!</strong>" . $e->getMessage() . "
                    </div>";
    }
}

######################### fim Atualizar #######################################
#
#
######################### inicio Deletar ######################################

function deletar($id) {

    $db = Conexao::abrir();
    $sqlDelete = 'DELETE FROM tb_categoria WHERE id = :id';
    try {
        $delete = $db->prepare($sqlDelete);
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        if ($delete->execute()) {
            echo "<div class='alert alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>&times;</button>
                            <strong>Deletado com sucesso!</strong>
                            </div>";
        }
    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Erro ao deletar!</strong>" . $e->getMessage() . "
                    </div>";
    }
}
######################### fim Deletar #######################################
#
#
######################### inicio Carregar um Usuario ########################

function carregarCategoria($id) {

    $db = Conexao::abrir();
    $sqlSelect = 'SELECT * FROM tb_categoria WHERE id = :id';

    try {
        $select = $db->prepare($sqlSelect);
        $select->bindValue(':id', $id, PDO::PARAM_INT);
        $select->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    $result = $select->fetch(PDO::FETCH_OBJ);

    return $result;
}

#
######################### fim Carregar um Usuario ########################

