<?php

#### funcao que exibe erro da pagina ####
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

###### INCLUI PAGINAS ######
include './CategoriaDB.php';
$db = Conexao::abrir();
include '../funcoesPagina.php';

##### Verifica usuario Logado ####
verificarLogin();

###### CHAMA FUNCAO TOPO PAGINA###### 
chamarTopoPagina();

?>

<body>
    <div class="container">
        <?php
        ############ Inicio Menu #########
        chamarMenu();//funcao chama menu
        ############ fim Menu ############
        #
        ########################## INICIO ACOES PAGINA ###############################
        #
        ########################### INICIO INSERIR DADOS ###############################
        // função 'isset' verifica se existe valor no vetor da variavel $_POST enviar 
        if (isset($_POST['enviar'])) {
            //se exister valor pega o nome e email do form e armazena nas variaveis $nome e $email
            $nome = $_POST['nome'];

            //chamar funcao salvar
            salvar($nome);
        }
        #
        ############################ FIM INSERIR DADOS ###############################
        #
        #
        ############################# INICIO ATUALIZAR DADOS #########################
        #
        // se clicar  no botão alterar vem para esta tela
        if (isset($_POST['btnAtualizar'])) {
            $id = (int) $_GET['id'];
            $nome = $_POST['nome'];

            //funcao atualizar
            atualizar($nome,$id);
        }
        ######################### FIM ATUALIZAR DADOS ################################
        #
        #
        ######################### INICIO DELETAR DADOS ###############################

        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $id = (int) $_GET['id'];

            //chamar funcao deletar
            deletar($id);
        }
        ######################### FIM DELETAR DADOS ###############################
        ?>

    <article>

        <section class="jumbotron">
            <?php
            ######################### INICIO FORMULARIO ATUALIZAR #######################################
            if (isset($_GET['action']) && $_GET['action'] == 'update') {

                $id = (int) $_GET['id'];

               $result = carregarCategoria($id);
                ?>

                <ul class="breadcrumb">
                    <li><a href="../index.php">Página inicial <span class="divider"> /</span> </a></li>
                    <li class="active">Atualizar</li>
                </ul>
                <h3>Formulario Categoria </h3>
                <form method="post" action="">
                    <label>Nome</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input type="text" name="nome" value="<?php echo $result->nome; ?>" placeholder="Nome:" />
                    </div>
                    
                    <br />
                    <input type="submit" name="btnAtualizar" class="btn btn-primary" value="Atualizar dados">					
                </form>

            <?php 
            ######################### FIM FORMULARIO ATUALIZAR  ##########################
            #
            #
            ######################### INICIO FORMULARIO CADASTRAR #######################
            
            } else {
                
                ?>
                <h3>Formulario Categoria </h3>
                <form method="post" action="">
                    <label>Nome</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input type="text" name="nome" placeholder="Nome:" />
                    </div>
                    
                    <br />
                    <input type="submit" name="enviar" class="btn btn-primary" value="Cadastrar dados">					
                </form>

            <?php
             }//fim
             ######################### FIM FORMULARIO CADASTRAR ####################
             #
             #
             ######################### INICIO LISTAGEM DADOS #######################
                    
            ?>
            <table class="table table-hover">   
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome:</th>
                        <th>Ações:</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                   ######### INICIO CARREGAR TODOS OS DADOS #########
                    //carrega todos os dados da tabela banco de dados
                    $sqlRead = 'SELECT * FROM tb_categoria';
                    try {
                        $read = $db->prepare($sqlRead);
                        $read->execute();
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                    }
                    while ($rs = $read->fetch(PDO::FETCH_OBJ)) {
                        ?>
                        <tr>
                            <td><?php echo $rs->id; ?></td>
                            <td><?php echo $rs->nome; ?></td>
                            <td>
                                <a href="categoriaFormList.php?action=update&id=<?php echo $rs->id; ?>" class="btn"><i class="icon-pencil"></i></a>
                                <a href="categoriaFormList.php?action=delete&id=<?php echo $rs->id; ?>" class="btn" onclick="return confirm('Deseja deletar?');"><i class="icon-remove"></i></a>
                            </td>
                        </tr>
                    <?php
                    }
                    ####### FIM CARREGAR DADOS #######
                    ?>
                </tbody>
            </table>
            <!----------------------- FIM LISTAGEM DADOS  ----------------------->
        </section>

    </article>

</div>
<?php 
###### CHAMA FUNCAO FIM PAGINA ######
chamarRodape();
