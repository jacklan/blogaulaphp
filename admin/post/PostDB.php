<?php

###### INCLUI PAGINAS ######
//include "../Conexao.class.php";
include dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "Conexao.class.php";

############## pagina funcoes de trasacao com banco de dados Post.##########
#
######################### inicio Salvar #######################################

function salvar($nome, $categoria_id, $datacadastro, $texto, $publicar) {
 
    $datacadastro = dataus($datacadastro);

    $db = Conexao::abrir();
    //comando SQL do banco de dados para inserir registros
    $sql = 'INSERT INTO tb_post (nome, categoria_id,datacadastro,texto,publicar) VALUES (:nome, :categoria_id,:datacadastro,:texto,:publicar)';
    //try catch tratamento de exceção
    try {
        //variavel $db criada na pagina Conexao.class.php sendo chamada nesta pagina
        //pega o valor da variavel SQL passando como parametro na função prepare
        $create = $db->prepare($sql);
        //chama a função bindValue passando o parametro nome e categoria_id do comando SQL e o valor da variavel
        $create->bindValue(':nome', $nome, PDO::PARAM_STR);
        $create->bindValue(':categoria_id', $categoria_id, PDO::PARAM_INT);
        $create->bindValue(':datacadastro', $datacadastro, PDO::PARAM_STR);
        $create->bindValue(':texto', $texto, PDO::PARAM_STR);
        $create->bindValue(':publicar', $publicar, PDO::PARAM_INT);
        // execulta os valores passados no parametro
        if ($create->execute()) {
            //mostra um alerta na tela de sucesso
            echo "<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Inserido com sucesso!</strong>
                    </div>";
        }
    } catch (PDOException $e) {
        // se caso de algum erro vem para o tratamento de exceção
        echo "<div class='alert alert-error'>
                <button type='button' class='close' data-dismiss='alert'>&times;</button>
                <strong>Erro ao inserir dados!</strong>" . $e->getMessage() . "
                </div>";
    }
}

######################### fim Salvar ##########################################
#
#
######################### inicio Atualizar ####################################

function atualizar($nome, $categoria_id, $datacadastro, $texto, $publicar, $id) {

    if ($publicar != 1 || $publicar === null) {
        $publicar = 0;
    }
    $datacadastro = dataus($datacadastro);

    $db = Conexao::abrir();
    //comando SQL para atualização do formulario
    $sqlUpdate = 'UPDATE tb_post SET nome = ?, categoria_id = ?, datacadastro = ?, texto = ?, publicar = ? WHERE id = ?';
    //cria um array recebendo os parametros do formulario de acordo com a sequencia do comando SQL
    $dados = array($nome, $categoria_id, $datacadastro, $texto, $publicar, $id);
    try {
        //
        $update = $db->prepare($sqlUpdate);
        if ($update->execute($dados)) {
            echo "<div class='alert alert-success'>
                        <button type='button' class='close' data-dismiss='alert'>&times;</button>
                        <strong>Atualizado com sucesso!</strong>
                        </div>";
        }
    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Erro ao atualizar!</strong>" . $e->getMessage() . "
                    </div>";
    }
}

######################### fim Atualizar #######################################
#
#
######################### inicio Deletar ######################################

function deletar($id) {

    $db = Conexao::abrir();
    $sqlDelete = 'DELETE FROM tb_post WHERE id = :id';
    try {
        $delete = $db->prepare($sqlDelete);
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        if ($delete->execute()) {
            echo "<div class='alert alert-success'>
                            <button type='button' class='close' data-dismiss='alert'>&times;</button>
                            <strong>Deletado com sucesso!</strong>
                            </div>";
        }
    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Erro ao deletar!</strong>" . $e->getMessage() . "
                    </div>";
    }
}

######################### fim Deletar #######################################
#
#
######################### inicio Carregar um Post ########################

function carregarPost($id) {

    $db = Conexao::abrir();
    $sqlSelect = 'SELECT * FROM tb_post WHERE id = :id';

    try {
        $select = $db->prepare($sqlSelect);
        $select->bindValue(':id', $id, PDO::PARAM_INT);
        $select->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    $result = $select->fetch(PDO::FETCH_OBJ);

    return $result;
}

#
######################### fim Carregar um Post ########################
#
######################### inicio Carregar uma categoria ########################

function carregarNomeCategoria($categoria_id) {

    $db = Conexao::abrir();
    $sqlSelect = 'SELECT c.nome FROM tb_categoria c WHERE id = :categoria_id';

    try {
        $select = $db->prepare($sqlSelect);
        $select->bindValue(':categoria_id', $categoria_id, PDO::PARAM_INT);
        $select->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    $result = $select->fetch(PDO::FETCH_OBJ);

    return $result->nome;
}
######################### fim Carregar uma categoria ########################
