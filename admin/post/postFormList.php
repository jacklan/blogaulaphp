<?php

#### funcao que exibe erro da pagina ####
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

###### INCLUI PAGINAS ######
include './PostDB.php';
$db = Conexao::abrir();
include '../funcoesPagina.php';

##### Verifica usuario Logado ####
verificarLogin();

###### CHAMA FUNCAO TOPO PAGINA###### 
chamarTopoPagina();

?>

<body>
    <div class="container">
        <?php
        ############ Inicio Menu #########
        chamarMenu();//funcao chama menu
        ############ fim Menu ############
        #
        ########################## INICIO ACOES PAGINA ###############################
        #
        ########################### INICIO INSERIR DADOS ###############################
        // função 'isset' verifica se existe valor no vetor da variavel $_POST enviar 
        if (isset($_POST['enviar'])) {
            //se exister valor pega o nome e categoria_id do form e armazena nas variaveis $nome e $categoria_id
            $nome = $_POST['nome'];
            $categoria_id = $_POST['categoria_id'];
            $datacadastro = $_POST['datacadastro'];
            $texto = $_POST['texto'];
            $publicar = $_POST['publicar'];
            
            if ($publicar != 1 || $publicar == null) {
                $publicar = 0;
            }
            
            //chamar funcao salvar
            salvar($nome, $categoria_id, $datacadastro,$texto,$publicar);
        }
        #
        ############################ FIM INSERIR DADOS ###############################
        #
        #
        ############################# INICIO ATUALIZAR DADOS #########################
        #
        // se clicar  no botão alterar vem para esta tela
        if (isset($_POST['btnAtualizar'])) {
            $id = (int) $_GET['id'];
            $nome = $_POST['nome'];
            $categoria_id = $_POST['categoria_id'];
            $datacadastro = $_POST['datacadastro'];
            $texto = $_POST['texto'];
            $publicar = $_POST['publicar'];
            
            if ($publicar != 1 || $publicar == null) {
                $publicar = 0;
            }
            
            //funcao atualizar
            atualizar($nome, $categoria_id, $datacadastro,$texto,$publicar ,$id);
        }
        ######################### FIM ATUALIZAR DADOS ################################
        #
        #
        ######################### INICIO DELETAR DADOS ###############################

        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $id = (int) $_GET['id'];

            //chamar funcao deletar
            deletar($id);
        }
        ######################### FIM DELETAR DADOS ###############################
        ?>

    <article>

        <section class="jumbotron">
            <?php
            ######################### INICIO FORMULARIO ATUALIZAR #######################################
            if (isset($_GET['action']) && $_GET['action'] == 'update') {

                $id = (int) $_GET['id'];

               $result = carregarPost($id);
                ?>

                <ul class="breadcrumb">
                    <li><a href="../index.php">Página inicial <span class="divider"> /</span> </a></li>
                    <li class="active">Atualizar</li>
                </ul>
                <h3>Formulario Poste</h3>
                <form method="post" action="">
                    <label>Titulo</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input type="text" name="nome" value="<?php echo $result->nome; ?>" placeholder="Nome:" />
                    </div>
                    <label>Data Publica&ccedil;&atilde;o</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="date" name="datacadastro"  value="<?php echo databr($result->datacadastro); ?>"  placeholder="Idade:"  />
                    </div>
                    <label>Categoria</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                            <select name="categoria_id" id="categoria_id">
                             <?php
                            $sqlRead = 'SELECT * FROM tb_categoria';
                            try {
                                $read = $db->prepare($sqlRead);
                                $read->execute();
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                            }

                            while ($rs = $read->fetch(PDO::FETCH_OBJ)) {
                            ?>
                                <option value="<?php echo $rs->id; ?>"><?php echo $rs->nome; ?></option>
                            <?php 
                            }
                            ?>
                            </select>
                    </div>
                    <label>Texto</label>
                    <div class="input-prepend">
                         <span class="add-on"><i class="icon-envelope"></i></span>
                        <textarea name="texto" rows="3"  cols="5"  value="<?php echo $result->texto; ?>" ><?php echo $result->texto; ?></textarea>
                    </div>
                    <label>Publicar</label>Sim ou n&atilde;o
                    <div class="input-prepend">
                        <?php
                        if($result->publicar == 1 && $result->publicar !=null){
                            ?>
                        <input type="checkbox" name="publicar" checked="checked" class="btn btn-block" value="1"/>
                        
                        <?php } else { ?>
                        
                             <input type='checkbox' name='publicar' class='btn btn-block' value="1"/>
                             
                        <?php  }   ?>
                    </div>
                    <br />
                    <input type="submit" name="btnAtualizar" class="btn btn-primary" value="Atualizar dados">					
                </form>

            <?php 
            ######################### FIM FORMULARIO ATUALIZAR  ##########################
            #
            #
            ######################### INICIO FORMULARIO CADASTRAR #######################
            
            } else {
                
                ?>
                <h3>Formulario Poste</h3>
                <form method="post" action="">
                    <label>Titulo</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input type="text" name="nome" placeholder="Titulo" />
                    </div>
                    <label>Data Publica&ccedil;&atilde;o</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="date" name="datacadastro" placeholder="00/00/2000" />
                    </div>
                    <label>Categoria</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                            <select name="categoria_id" id="categoria_id">
                             <?php
                            $sqlRead = 'SELECT * FROM tb_categoria';
                            try {
                                $read = $db->prepare($sqlRead);
                                $read->execute();
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                            }

                            while ($rs = $read->fetch(PDO::FETCH_OBJ)) {
                            ?>
                                <option value="<?php echo $rs->id; ?>"><?php echo $rs->nome; ?></option>
                            <?php 
                            }
                            ?>
                            </select>
                    </div>
                    <label>Texto</label>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <textarea name="texto" placeholder="Insira seu publicar:" rows="3"  cols="5"></textarea>
                    </div>
                    <br />
                    <label>Publicar</label>Sim ou n&atilde;o
                    <div class="input-prepend">
                        <input type="checkbox" name="publicar" class="btn btn-block" value="1">				
                    </div><br />
                    <input type="submit" name="enviar" class="btn btn-primary" value="Cadastrar dados">					
                </form>

            <?php
             }//fim
             ######################### FIM FORMULARIO CADASTRAR ####################
             #
             #
             ######################### INICIO LISTAGEM DADOS #######################
                    
            ?>
            <table class="table table-hover">   
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome:</th>
                        <th>Categoria:</th>
                        <th>Data:</th>
                        <th>Publicado:</th>
                        <th>Ações:</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                   ######### INICIO CARREGAR TODOS OS DADOS #########
                    //carrega todos os dados da tabela banco de dados
                    $sqlRead = 'SELECT * FROM tb_post';
                    try {
                        $read = $db->prepare($sqlRead);
                        $read->execute();
                    } catch (PDOException $e) {
                        echo $e->getMessage();
                    }
                    while ($rs = $read->fetch(PDO::FETCH_OBJ)) {
                        ?>
                        <tr>
                            <td><?php echo $rs->id; ?></td>
                            <td><?php echo $rs->nome; ?></td>
                            <td><?php echo carregarNomeCategoria( $rs->categoria_id ) ?></td>
                            <td><?php echo databr($rs->datacadastro); ?></td>
                            <td><?php echo $rs->publicar; ?></td>
                            <td>
                                <a href="postFormList.php?action=update&id=<?php echo $rs->id; ?>" class="btn"><i class="icon-pencil"></i></a>
                                <a href="postFormList.php?action=delete&id=<?php echo $rs->id; ?>" class="btn" onclick="return confirm('Deseja deletar?');"><i class="icon-remove"></i></a>
                            </td>
                        </tr>
                    <?php
                    }
                    ####### FIM CARREGAR DADOS #######
                    ?>
                </tbody>
            </table>
            <!----------------------- FIM LISTAGEM DADOS  ----------------------->
        </section>

    </article>

</div>
<?php 
###### CHAMA FUNCAO FIM PAGINA ######
chamarRodape();
