-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 31-Mar-2016 às 23:15
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12


--
-- Database: `db_projeto`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categoria`
--

CREATE TABLE IF NOT EXISTS `tb_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `tb_categoria`
--

INSERT INTO `tb_categoria` (`id`, `nome`) VALUES
(3, 'Categoria 2'),
(4, 'Categoria 3'),
(5, 'Categoria 1'),
(6, 'categoria 4');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_contato`
--

CREATE TABLE IF NOT EXISTS `tb_contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `assunto` varchar(80) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `tb_contato`
--

INSERT INTO `tb_contato` (`id`, `nome`, `email`, `assunto`, `texto`) VALUES
(1, 'teste update', '4', '2016-02-02 00:00:00', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_post`
--

CREATE TABLE IF NOT EXISTS `tb_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `datacadastro` datetime NOT NULL,
  `publicar` char(1) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Extraindo dados da tabela `tb_post`
--

INSERT INTO `tb_post` (`id`, `nome`, `categoria_id`, `datacadastro`, `publicar`, `usuario_id`, `texto`) VALUES
(1, 'teste2', 4, '2016-03-04 00:00:00', '0', NULL, '   teste2'),
(3, 'ApresentaÃ§Ã£o da Disciplina', 3, '2016-02-02 00:00:00', '', NULL, '  teset1 texto'),
(4, 'teste', 6, '2015-02-02 00:00:00', '1', NULL, 'teste texto'),
(21, 'teste post2', 4, '2016-03-03 00:00:00', '0', NULL, '  teste2'),
(24, 'teste post', 6, '0000-00-00 00:00:00', '1', NULL, 'teste'),
(26, 'teste post', 4, '2016-02-02 00:00:00', '1', NULL, 'teste'),
(27, 'teste post', 3, '2015-02-02 00:00:00', '1', NULL, 'teste'),
(28, 'teste post', 3, '2015-02-02 00:00:00', '1', NULL, 'teste'),
(29, 'teste post', 3, '2015-02-02 00:00:00', '1', NULL, 'teste'),
(30, 'teste post', 3, '2015-02-02 00:00:00', '1', NULL, 'teste'),
(31, 'teste post', 6, '2015-02-02 00:00:00', '1', NULL, 'TESTE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_slideshow`
--

CREATE TABLE IF NOT EXISTS `tb_slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) COLLATE latin1_bin NOT NULL,
  `descricao` varchar(100) COLLATE latin1_bin DEFAULT NULL,
  `arquivo_foto` varchar(80) COLLATE latin1_bin DEFAULT '0',
  `link` varchar(200) COLLATE latin1_bin DEFAULT NULL,
  `publicar` char(1) COLLATE latin1_bin NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_bin AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `tb_slideshow`
--

INSERT INTO `tb_slideshow` (`id`, `titulo`, `descricao`, `arquivo_foto`, `link`, `publicar`) VALUES
(1, 'teste2', 'teste', '30_03_2016_18_53_09.jpg', '#', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuario`
--

CREATE TABLE IF NOT EXISTS `tb_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `idade` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `tb_usuario`
--

INSERT INTO `tb_usuario` (`id`, `nome`, `email`, `idade`) VALUES
(1, 'Jackson', 'contato@jacksonsistemas.com.br', 0),
(3, 'teste 02', 'teste.com.br', 0),
(7, 'Aula PHP', 'lordjackson@gmail.com', 28),
(8, 'Aula PHP', 'lordjackson@gmail.com', 28),
(10, 'Luan', 'marialucia.dantas59@hotmail.com', 22),
(11, 'Jose Pedro', 'brundler@outlook.com', 15),
(14, 'Welliton2', 'welliton@eaj.com2', 26);

