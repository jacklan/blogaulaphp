
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
//session_start();

include './admin/post/PostDB.php';
include './admin/funcoesPagina.php';

$db = Conexao::abrir();
?>
<html>
    <head>
        <title>TCC - T&eacute;nico UFRN</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Business_Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link href="css/style.css" rel='stylesheet' type='text/css' />	
        <!-- css SlideShow -->
        <link rel="stylesheet" href="slideshow/dist/css/unslider.css">
        <!-- fim css SlideShow -->
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <!--start-main-->
        <div class="header">
            <div class="header-top">
                <div class="container">
                    <div class="logo">
                        <a href="index.php"><h1>BLOG Projeto Base TCC - Tecnico Inform&aacute;tica EAJ 2014.2</h1></a>
                    </div>
                    <div class="search">
                        <form>
                            <input type="text" value="Pesquisar" onfocus="this.value = '';" onblur="if (this.value == '') {
                                        this.value = 'Pesquisar';
                                    }">
                            <input type="submit" value="">
                        </form>
                    </div>
                    <div class="social">
                        <ul>
                            <li><a href="#" class="facebook"> </a></li>
                            <li><a href="#" class="facebook twitter"> </a></li>
                            <li><a href="#" class="facebook chrome"> </a></li>
                            <li><a href="#" class="facebook in"> </a></li>
                            <li><a href="#" class="facebook beh"> </a></li>
                            <li><a href="#" class="facebook vem"> </a></li>
                            <li><a href="#" class="facebook yout"> </a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <!--head-bottom-->
            <div class="head-bottom">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Inicio</a></li>
                            <li><a href="#">Sobre</a></li>
                            <li><a href="#">Reviews</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tech <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="tech.html">Action</a></li>
                                    <li><a href="tech.html">Action</a></li>
                                    <li><a href="tech.html">Action</a></li>
                                </ul>
                            </li><!--
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Culture <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="singlepage.html">Action</a></li>
                                    <li><a href="singlepage.html">Action</a></li>
                                    <li><a href="singlepage.html">Action</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Science <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="singlepage.html">Action</a></li>
                                    <li><a href="singlepage.html">Action</a></li>
                                    <li><a href="singlepage.html">Action</a></li>
                                </ul>
                            </li>
                            <li><a href="design.html">Design</a></li>
                            <li><a href="business.html">Business</a></li>
                            <li><a href="world.html">World</a></li>
                            <li><a href="forum.html">Forum</a></li>
                            -->
                            <li><a href="contato.php">Contato</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
                </nav>
            </div>
            <!--head-bottom-->
        </div>	
        <!-- banner -->
        <div class="banner">
            <div class="container">
                <div class="my-slider">
                    <ul>
                        <?php
################## Carregar Slideshow ############################
//carrega todos os dados da tabela banco de dados
                        $sqlReadSS = "SELECT * FROM tb_slideshow where publicar = 1 order by id desc";
// $sqlReadSS = "SELECT * FROM tb_slideshow s where s.publicar=1 order by id desc limit 3 ";
                        try {
                            $readSS = $db->prepare($sqlReadSS);
                            $readSS->execute();
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                        }
### inicio laço ###
                        while ($rs = $readSS->fetch(PDO::FETCH_OBJ)) {
                            ?>
                            <li> 
                                <h2><?php echo $rs->titulo; ?></h2>
                                <p style="margin-left: 400px;"><?php echo $rs->descricao; ?></p>

                                <?php
                                if (file_exists("admin/slideshow/imagens/" . $rs->arquivo_foto)) {
                                    ?>
                                    <div id="imageSlidShow" style="height: 150px;">
                                        <img src="admin/slideshow/imagens/<?php echo $rs->arquivo_foto; ?>" 
                                             alt="" style="float: left; margin-bottom: -100px; 
                                             margin-top: -150px; margin-left: 25px; width: 380px; 
                                             height: 250px;"/>
                                    </div>
                                    <?php
                                }
                                ?>
                                <a  style="float: left;  margin-left: 501px; margin-top: -80px;" href="<?php echo $rs->link; ?>" target="_black">Leia Mais</a>
                            </li>
                            <?php
                        }
                        ?>

                    </ul>
                </div>

            </div>
        </div>
        <!-- technology -->
        <div class="technology">
            <!-- inicio conteudo -->
            <div class="container">
                <div class="col-md-9 technology-left">
                    <div class="tech-no">
                        <?php
################## Carregar Pots ############################
//carrega todos os dados da tabela banco de dados
                        $sqlRead = 'SELECT * FROM tb_post order by id desc limit 3 ';
                        try {
                            $read = $db->prepare($sqlRead);
                            $read->execute();
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                        }
### inicio laço ###
                        while ($rs = $read->fetch(PDO::FETCH_OBJ)) {
                            ?>
                            <!-- technology-top -->
                            <div class="soci">
                                <ul>
                                    <li><a href="#" class="facebook-1"> </a></li>
                                    <li><a href="#" class="facebook-1 twitter"> </a></li>
                                    <li><a href="#" class="facebook-1 chrome"> </a></li>
                                    <li><a href="#"><i class="glyphicon glyphicon-envelope"> </i></a></li>
                                    <li><a href="#"><i class="glyphicon glyphicon-print"> </i></a></li>
                                    <li><a href="#"><i class="glyphicon glyphicon-plus"> </i></a></li>
                                </ul>
                            </div>
                            <div class="tc-ch">
                                <div class="tch-img">
                                    <a href="singlepage.html"><img src="images/1.jpg" class="img-responsive" alt=""/></a>
                                </div>

                                <a class="blog blue" href="singlepage.html"><?php echo carregarNomeCategoria($rs->categoria_id) ?></a>
                                <h3><a href="singlepage.html"><?php echo $rs->nome; ?></a></h3>

                                <p><?php echo $rs->texto; ?></p>


                                <div class="blog-poast-info">
                                    <ul>
                                        <li><i class="glyphicon glyphicon-user"> </i><a class="admin" href="#"> Admin </a></li>
                                        <li><i class="glyphicon glyphicon-calendar"> </i><?php echo databr($rs->datacadastro); ?></li>
                                        <li><i class="glyphicon glyphicon-comment"> </i><a class="p-blog" href="#">3 Comments </a></li>
                                        <li><i class="glyphicon glyphicon-heart"> </i><a class="admin" href="#">5 favourites </a></li>
                                        <li><i class="glyphicon glyphicon-eye-open"> </i>1.128 views</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- technology-top -->
                            <?php
                        }// fim laço ###
####### FIM CARREGAR DADOS #######
                        ?>
                        <div class="clearfix"></div>
                        <!-- technology-top -->
                        <div class="soci">
                            <ul>
                                <li><a href="#" class="facebook-1"> </a></li>
                                <li><a href="#" class="facebook-1 twitter"> </a></li>
                                <li><a href="#" class="facebook-1 chrome"> </a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-envelope"> </i></a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-print"> </i></a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-plus"> </i></a></li>
                            </ul>
                        </div>

                        <div class="tc-ch">

                            <div class="tch-img">
                                <a href="singlepage.html"><img src="images/2.jpg" class="img-responsive" alt=""/></a>
                            </div>
                            <a class="blog pink" href="singlepage.html">Science</a>
                            <h3><a href="singlepage.html">Lorem Ipsum is simply</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud eiusmod tempor incididunt ut labore et dolore magna aliqua exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <p>Ut enim ad minim veniam, quis nostrud eiusmod tempor incididunt ut labore et dolore magna aliqua exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                            <div class="blog-poast-info">
                                <ul>
                                    <li><i class="glyphicon glyphicon-user"> </i><a class="admin" href="#"> Admin </a></li>
                                    <li><i class="glyphicon glyphicon-calendar"> </i>30-12-2015</li>
                                    <li><i class="glyphicon glyphicon-comment"> </i><a class="p-blog" href="#">3 Comments </a></li>
                                    <li><i class="glyphicon glyphicon-heart"> </i><a class="admin" href="#">5 favourites </a></li>
                                    <li><i class="glyphicon glyphicon-eye-open"> </i>1.128 views</li>
                                </ul>
                            </div>
                        </div>

                        <!-- technology-top -->

                    </div>
                </div>
                <!-- technology-right -->
                <div class="col-md-3 technology-right">
                    <div class="blo-top">
                        <div class="tech-btm">
                            <img src="images/banner1.jpg" class="img-responsive" alt=""/>
                        </div>
                    </div>
                    <div class="blo-top">
                        <div class="tech-btm">
                            <h4>Sign up to our newsletter</h4>
                            <p>Pellentesque dui, non felis. Maecenas male</p>
                            <div class="name">
                                <form>
                                    <input type="text" placeholder="Email" required="">
                                </form>
                            </div>	
                            <div class="button">
                                <form>
                                    <input type="submit" value="Subscribe">
                                </form>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="blo-top1">
                        <div class="tech-btm">
                            <h4>Top stories of the week </h4>
                            <div class="blog-grids">
                                <div class="blog-grid-left">
                                    <a href="singlepage.html"><img src="images/6.jpg" class="img-responsive" alt=""/></a>
                                </div>
                                <div class="blog-grid-right">

                                    <h5><a href="singlepage.html">Pellentesque dui, non felis. Maecenas male</a> </h5>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="blog-grids">
                                <div class="blog-grid-left">
                                    <a href="singlepage.html"><img src="images/7.jpg" class="img-responsive" alt=""/></a>
                                </div>
                                <div class="blog-grid-right">

                                    <h5><a href="singlepage.html">Pellentesque dui, non felis. Maecenas male</a> </h5>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="blog-grids">
                                <div class="blog-grid-left">
                                    <a href="singlepage.html"><img src="images/11.jpg" class="img-responsive" alt=""/></a>
                                </div>
                                <div class="blog-grid-right">

                                    <h5><a href="singlepage.html">Pellentesque dui, non felis. Maecenas male</a> </h5>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="blog-grids">
                                <div class="blog-grid-left">
                                    <a href="singlepage.html"><img src="images/9.jpg" class="img-responsive" alt=""/></a>
                                </div>
                                <div class="blog-grid-right">

                                    <h5><a href="singlepage.html">Pellentesque dui, non felis. Maecenas male</a> </h5>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="blog-grids">
                                <div class="blog-grid-left">
                                    <a href="singlepage.html"><img src="images/10.jpg" class="img-responsive" alt=""/></a>
                                </div>
                                <div class="blog-grid-right">

                                    <h5><a href="singlepage.html">Pellentesque dui, non felis. Maecenas male</a> </h5>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <!-- technology-right -->
            </div>
        </div>
        <!-- technology -->
        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="col-md-4 footer-left">
                    <h6>THIS LOOKS GREAT</h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt consectetur adipisicing elit,</p>
                </div>
                <div class="col-md-4 footer-middle">
                    <h4>Twitter Feed</h4>
                    <div class="mid-btm">
                        <p>Consectetur adipisicing</p>
                        <p>Sed do eiusmod tempor</p>
                        <a href="https://w3layouts.com/">https://w3layouts.com/</a>
                    </div>

                    <p>Consectetur adipisicing</p>
                    <p>Sed do eiusmod tempor</p>
                    <a href="https://w3layouts.com/">https://w3layouts.com/</a>

                </div>
                <div class="col-md-4 footer-right">
                    <h4>Quick Links</h4>
                    <li><a href="#">Eiusmod tempor</a></li>
                    <li><a href="#">Consectetur </a></li>
                    <li><a href="#">Adipisicing elit</a></li>
                    <li><a href="#">Eiusmod tempor</a></li>
                    <li><a href="#">Consectetur </a></li>
                    <li><a href="#">Adipisicing elit</a></li>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- footer -->
        <!-- footer-bottom -->
        <div class="foot-nav">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="singlepage.html">Sobre</a></li>
                    <li><a href="videos.html">Videos</a></li>
                    <li><a href="reviews.html">Reviews</a></li>
                    <li><a href="tech.html">Tech</a></li>
                    <!--<li><a href="singlepage.html">Science</a></li>
                    <li><a href="design.html">Design</a></li>
                    <li><a href="business.html">Business</a></li>
                    <li><a href="world.html">World</a></li>
                    <li><a href="forum.html">Forum</a></li> -->
                    <li><a href="contato.php">Contato</a></li>
                    <div class="clearfix"></div>
                </ul>
            </div>
        </div>
        <!-- footer-bottom -->
        <div class="copyright">
            <div class="container">
                <p>&copy; 2016 Business_Blog. All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
            </div>
        </div>


        <!-- There'll be a load of other stuff here -->
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="slideshow/src/js/unslider.js"></script> <!-- but with the right path! -->

        <script>
                                jQuery(document).ready(function ($) {
                                    $('.my-slider').unslider();
                                });
        </script>
    </body>
</body>
</html>